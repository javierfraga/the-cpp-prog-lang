#include "Vector.h"  // get Vector’s interface
#include <cmath>     // get the the standard-librar y math function interface
#include <iostream>
using namespace std; // make std members visible (§2.4.2)

double sqrt_sum(Vector& v)
{
    // allocate s elements for v
    for (int i=0; i!=5; ++i){
        std::cout << "Enter a number:";
        cin>>v[i];
    }

    double sum = 0;
    for (int i=0; i!=v.size(); ++i)
        sum+=sqrt(v[i]);
    return sum;
}

int main(void)
{
    Vector v(5);

    std::cout << "Here is the square root:" << sqrt_sum(v) << std::endl;
    
    return 0;
}
