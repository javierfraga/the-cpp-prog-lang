#include <iostream>
#include <cmath>

int main(void)
{

    auto b = true;// a bool
    auto ch = 'x'; // a char
    auto i = 123; // an int
    auto d = 1.2; // a double
    auto y = 3.14;
    auto z = sqrt(y); // z has the type of whatever sqrt(y) retur ns

    std::cout << "b:" << b << std::endl;
    std::cout << "ch:" << ch << std::endl;
    std::cout << "i:" << i << std::endl;
    std::cout << "d:" << d << std::endl;
    std::cout << "z:" << z << std::endl;


    return 0;
}
