#include <iostream>
using namespace std;

class Vector {
    public:
        Vector(int s) :elem{new double[s]}, sz{s} { }
        double& operator[](int i) { return elem[i]; }
        int size() { return sz; }
    private:
        double* elem; // pointer to the elements
        int sz;       // the number of elements
};
    // read s integers from cin and return their sum; s is assumed to be positive
double read_and_sum(int s)
{
    Vector v(s);
    // allocate s elements for v
    for (int i=0; i!=s; ++i){
        std::cout << "Enter a number:";
        cin>>v[i];
    }
    // read into elements
    double sum = 0;
    for (int i=0; i!=s; ++i)
        sum+=v[i];
    return sum;
}

int main(void)
{

    std::cout << "here is answer:" << read_and_sum(5) << std::endl; 

    return 0;
}
