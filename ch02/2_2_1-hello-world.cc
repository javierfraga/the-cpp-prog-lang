//#include <iostream>

//int main(void)
//{
    //std::cout << "Hello World!\n";
    //return 0;
//}

#include <iostream>
using namespace std;

double square( double x )
{
    return x*x;
}

void print_square( double x )
{
    cout << "the square of " << x << " is " << square(x) << "\n" << endl;
}

int main(void)
{
    print_square(1.234);
    return 0;
}
