#include <iostream>
#include <boost/range/adaptor/reversed.hpp>
using namespace std;
using namespace boost::adaptors;

int main(void)
{
    int v1[10] = {0,1,2,3,4,5,6,7,8,9};
    char *string= (char *)"javier";

    /*
     *Arrays and objects have special foreach function
     */
    std::cout << "Arrays and objects have special foreach function " << std::endl;
    std::cout << std::endl;

    /*
     *Let's see what happens after I increment like this
     */
    for (auto x : v1) {
        ++x; // Nothing happens
    }
    /*
     *Output here is unchanged from previous foreach loop
     */
    std::cout << "Foreach loop by reference" << std::endl;
    for (auto &x : v1) {
        std::cout << x << std::endl;
    }
    std::cout << "-----" << std::endl;

    /*
     *Let's see what happens after I increment like this
     */
    for (auto &x : v1) {
        ++x; // By reference, changes value
    }
    /*
     *Output here is modified as we passed by reference into previous foreach
     */
    std::cout << "Once again foreach loop but incremented previously by reference foreach loop" << std::endl;
    for (auto x : v1) {
        std::cout << x << std::endl;
    }
    std::cout << "-----" << std::endl;

    /*
     *In reverse order, either one works below
     */
    //for (auto i : reverse(v1)) {
    std::cout << "Reverse order foreach with boost library" << std::endl;
    for (auto i : v1 | reversed) {
        std::cout << i << std::endl;
    }
    std::cout << "-----" << std::endl;

    /*
     *Strings still have classic for loop, no foreach
     */
    std::cout << "Strings still have classic for loop, no foreach" << std::endl;
    for (; *string!=0; ++string){
        std::cout << *string << std::endl;    
    }
    std::cout << "-----" << std::endl;

    /*
     *Array can also do classic loop, tricky syntax
     */
    std::cout << "Array can also do classic loop, tricky syntax" << std::endl;
    int *end = v1 + 10;
    for (int *v2 = v1; v2 < end; ++v2) {
        std::cout << *v2 << std::endl;
    }

    std::cout << "-----" << std::endl;

    /*
     *This is easier syntactical way for classic array traverse
     */
    std::cout << "This is easier syntactical way for classic array traverse" << std::endl;
    for (int i = 0; i < 10; ++i) {
        std::cout << *(v1 + i) << std::endl;
    }

    return 0;
}
